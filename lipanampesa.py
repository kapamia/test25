import requests
from requests.auth import HTTPBasicAuth

from credentials import MpesaAccessToken, LipanaMpesaPpassword


def lipa_na_mpesa():
    access_token = MpesaAccessToken.validated_mpesa_access_token
    headers = {"Authorization": "Bearer %s" % access_token}
    api_url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"

    request = {
        "BusinessShortCode": LipanaMpesaPpassword.Business_short_code,
        "Password": LipanaMpesaPpassword.decode_password,
        "Timestamp": LipanaMpesaPpassword.lipa_time,
        "TransactionType": "CustomerPayBillOnline",
        "Amount": 1,
        "PartyA": 254790822647,  # replace with your phone number to get stk push
        "PartyB": LipanaMpesaPpassword.Business_short_code,
        "PhoneNumber": 254790822647,  # replace with your phone number to get stk push
        "CallBackURL": "https://kapamia.com/api/payments/lnm/",
        "AccountReference": "1271082047",
        "TransactionDesc": "Buy Ticket"
    }

    response = requests.post(api_url, json=request, headers=headers)

    print(response.text)


lipa_na_mpesa()
